import React, { Component } from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { StaticQuery } from "gatsby";
import { Picture, PictureTitle, PictureImage } from '../styledComponents/rysunek'
import { Modal, ModalContent } from '../styledComponents/modal'

class IndexPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedPicture: false,
    }
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.setState(prevState => ({
      selectedPicture: !prevState.selectedPicture
    }));
  }
render() {
  const modal = (<Modal onClick={this.handleClick} />)
  const modalContent = (<ModalContent src={this.state.selectedPicture} onClick={this.handleClick} />)

  return (<Layout>
    <SEO title="Home" />
    <div style={{display: 'flex', flexWrap: 'wrap'}}>
    <StaticQuery
      query={graphql`
        query {
            pictures {
              rysuneks {
                id
                opis
                dataNarysowania
                tytul
                rysunek {
                  url
                }
                oryginal {
                  zdjecie {
                    url
                  }
                }
              }
            }
        }
      `}
      render={data => (
        data.pictures.rysuneks.map(rysunek => {
          return (
              <Picture onClick={this.handleClick}>
                <PictureImage src={rysunek.rysunek.url} />
                <PictureTitle>{rysunek.tytul}</PictureTitle>
              </Picture>
          )
        }
      ))}
    />
    </div>
    {this.state.selectedPicture ? modal : ''}
    {this.state.selectedPicture ? modalContent : ''}
  </Layout>)
  }
}

export default IndexPage
