import styled, { keyframes } from 'styled-components'

const fade_in = keyframes`
    0% { opacity: 0; }
    100% { opacity: 1; }
`
const tracking_in_expand = keyframes`
    0% {
      letter-spacing: -0.5em;
      opacity: 0;
    }
    40% {
      opacity: 0.6;
    }
    100% {
      opacity: 1;
    }
`
export const Picture = styled.div`
  margin: 10px;
  width: 300px;
  box-shadow: 0 0 10px;
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 15px;
  animation: ${fade_in} 1.2s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
  &:hover {
    transition: background-color 0.3s, box-shadow 0.3s;
    background-color: #bbb;
    cursor: pointer;
    box-shadow: 0 0 20px;
  }
`

export const PictureTitle = styled.h3`
    text-align: center;
    animation: ${tracking_in_expand} 1.5s cubic-bezier(0.215, 0.610, 0.355, 1.000) both;
`

export const PictureImage = styled.img`
    width: 300px;
    border-radius: 5px;
`