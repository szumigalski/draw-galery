import styled, { keyframes } from 'styled-components'

const fade_in = keyframes`
    0% { opacity: 0; }
    100% { opacity: 1; }
`

const fade_in_bakcground = keyframes`
    0% { opacity: 0; }
    100% { opacity: 0.7; }
`

export const Modal = styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: black;
    z-index: 1;
    animation: ${fade_in_bakcground} 0.7s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
`

export const ModalContent = styled.img`
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    width: 600px;
    z-index: 200;
    animation: ${fade_in} 0.7s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
`